<!--
<?php
/*
$txt = "Hello World";
$number = 15;

echo $txt."<br>";
echo $number."<br>";

echo "<ha>This is a simple heading.</h4>";
echo "<h4 style='color: green;'>This is heading with style.</h4>";

$a = 123;
var_dump($a);
echo "<br>";
$color = array("Red", "green", "Blue");
 var_dump($color);
 echo "<br>";
 

class greeting{

	public $str = "Hello";

	function show_greeting(){
		return $this ->str;
	}
}

$message = new greeting;
var_dump($message ->show_greeting());
echo "<br>";
*/

?>
-->
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Example of PHP $_REQUEST variable</title>
</head>
<body>


<?php
if(isset($_REQUEST["name"])){
	echo "<p>Hi, ". $_REQUEST["name"] . "</p>";
}
?>

<form method="post" action="<?php echo $_SERVER["PHP_SELF"];?>">
	<label> for="inputName">Name:</label>
	<input type="text" name="name" id="inputName">
	<input type="submit" value="submit" >
</form>
</body>